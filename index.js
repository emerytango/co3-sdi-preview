const { app, BrowserWindow, webContents } = require('electron')
const { spawn } = require('child_process')
const path = require('path')
const ipc = require('electron').ipcMain
var win = null
const videoFormatRegex = /\[video\ dec\.\]\ /
let formatChange = []


if (handleSquirrelEvent(app)) {
    return
}

function createWindow() {
    win = new BrowserWindow({
        width: 700,
        height: 400,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false
        }
    })
    win.loadFile('index.html')
    // win.webContents.openDevTools()
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
    }
})

ipc.on('view loaded', () => {
    win.webContents.send('init view', 'version 0.0.15')
    win.webContents.send('app path', app.getAppPath())
})


ipc.on('six preview', (event, arg) => {
    const yooVee6 = spawn(`${app.getAppPath()}/UltraGrid-1.7-win64/uv.exe`, [
        '--verbose=4',
        '-d',
        'gl:size=35',
        '-r',
        'portaudio',
        arg,
        '--param',
        'window-title=6th Floor SDI Preview'
    ])

    yooVee6.stdout.on('data', (data) => {
        console.log(`stdout: ${data}`)
        win.webContents.send('uv message', data)
    });

    yooVee6.stderr.on('data', (data) => {
        if (videoFormatRegex.test(data)) {
            win.webContents.send('six stderr', data)
        }
    });

    yooVee6.on('close', (code) => {
        console.log(`child process exited with code ${code}`)
        win.webContents.send('exit code', {
            code: code,
            stream: 'yooVee6'
        })
    });
})


ipc.on('nine preview', (event, arg) => {
    const yooVee9 = spawn(`${app.getAppPath()}/UltraGrid-1.7-win64/uv.exe`, [
        '--verbose=4',
        '-d',
        'gl:size=35',
        '-r',
        'portaudio',
        '-P',
        arg,
        '--param',
        'window-title=9th Floor SDI Preview'
    ])

    yooVee9.stdout.on('data', (data) => {
        console.log(`stdout: ${data}`)
        win.webContents.send('uv message', data)
    });

    yooVee9.stderr.on('data', (data) => {
        if (videoFormatRegex.test(data)) {
            win.webContents.send('nine stderr', data)
        }
    });

    yooVee9.on('close', (code) => {
        console.log(`child process exited with code ${code}`)
        win.webContents.send('exit code', {
            code: code,
            stream: 'yooVee9'
        })
    });

})


ipc.on('close app', () => {
    app.quit()
})

function handleSquirrelEvent(application) {
    if (process.argv.length === 1) {
        return false;
    }

    const ChildProcess = require('child_process');

    const appFolder = path.resolve(process.execPath, '..');
    const rootAtomFolder = path.resolve(appFolder, '..');
    const updateDotExe = path.resolve(path.join(rootAtomFolder, 'Update.exe'));
    const exeName = path.basename(process.execPath);

    const spawn = function (command, args) {
        let spawnedProcess, error;

        try {
            spawnedProcess = ChildProcess.spawn(command, args, {
                detached: true
            });
        } catch (error) { }

        return spawnedProcess;
    };

    const spawnUpdate = function (args) {
        return spawn(updateDotExe, args);
    };

    const squirrelEvent = process.argv[1];
    switch (squirrelEvent) {
        case '--squirrel-install':
        case '--squirrel-updated':
            spawnUpdate(['--createShortcut', exeName]);

            setTimeout(application.quit, 1000);
            return true;

        case '--squirrel-uninstall':

            spawnUpdate(['--removeShortcut', exeName]);

            setTimeout(application.quit, 1000);
            return true;

        case '--squirrel-obsolete':

            application.quit();
            return true;
    }
};
