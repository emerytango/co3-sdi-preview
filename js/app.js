const ipc = require('electron').ipcRenderer
const { markTimeline } = require('console')
const {
  StringDecoder
} = require('string_decoder')
const decoder = new StringDecoder('utf8')
const appState = document.getElementById('appState')
const serverMessages = document.getElementById('serverMessages')
const motd = document.getElementById('motd')
const yooVee6 = document.getElementById('yooVee6')
const yooVee9 = document.getElementById('yooVee9')
const stderr6 = document.getElementById('stderr6')
const stderr9 = document.getElementById('stderr9')

const goodVibes = [
  "People underestimate you!",
  "Color assists are undervalued!",
  "You are the unsung hero of post production.",
  "It was my pleasure to serve you.",
  "You're good enough, smart enough, and people like you!!!",
  "You're beautiful on the inside and out.",
  "The only thing brighter than the projector is the light you bring",
  "Call someone you love today.",
  "Always be cool!",
  "You're doing a great job!",
  "Elon saved Twitter.",
  "The election was rigged.",
  "Free our Jan 6 patriots.",
  "NASA is lying to you.",
  "You are loved.",
  "Don't have a good day. Have a GREAT day!",
  "People like you",
  "An infinity symbol is called a lemniscate",
  "If you aint first, you're last",
  "If you see Erik today, tell hime you like his shirt",
  "Without struggle, there is no progress."
]


window.onload = function () {
  ipc.send('view loaded')
}


ipc.on('init view', (event, message) => {
  appState.innerText = message
})


ipc.on('server message', (event, message) => {
  serverMessages.style.overflow = 'clip'
  serverMessages.innerText = message.toString()
})


ipc.on('uv message', (event, message) => {
  // serverMessages.style.color = 'cadetblue'
  // serverMessages.style.fontSize = '.8em'
  serverMessages.style.overflow = 'clip'
  // serverMessages.innerText = decoder.write(message)
})

ipc.on('six stderr', (event, message) => {
  stderr6.style.fontSize = '.8em'
  stderr6.style.overflow = 'auto'
  stderr6.style.color = 'yellow'
  stderr6.innerText = `T1: ${decoder.write(message)}`
})

ipc.on('nine stderr', (event, message) => {
  stderr9.style.fontSize = '.8em'
  stderr9.style.overflow = 'auto'
  stderr9.style.color = 'cyan'
  stderr9.innerText = `T9: ${decoder.write(message)}`
})

ipc.on('exit code', (event, message) => {
  console.log(`stream: ${message.stream}`)
  console.log(`code: ${message.code}`)
  switch (message.stream) {
    case 'yooVee6':
      yooVee6.disabled = false
      break;
    case 'yooVee9':
      yooVee9.disabled = false
      break;
    default:
      console.log('no match')
      break;
  }
  if (message.code === 0) {
    serverMessages.style.color = 'green'
    serverMessages.style.fontSize = '1em'
    serverMessages.innerText = makeit(goodVibes)
  } else {
    serverMessages.innerText = `exit error: ${message.code}`
  }
})


yooVee6.addEventListener('click', () => {
  ipc.send('six preview', '10.208.79.132');
  yooVee6.disabled = true
})


yooVee9.addEventListener('click', () => {
  ipc.send('nine preview', '6000')
  yooVee9.disabled = true
})


document.getElementById('exit').addEventListener('click', () => {
  ipc.send('close app')
})

function makeit(arr) {
  let random = Math.floor(Math.random() * goodVibes.length)
  return goodVibes[random]
}